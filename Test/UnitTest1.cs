﻿using CalculadoraExtraordinaria2023;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Test
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestSumaDeProductos()
        {
            int result = Program.SumaDeProductos(5, 3);
            Assert.AreEqual(8, result);
        }

        [Test]
        public void TestRestaDeProductos()
        {
            int result = Program.RestaDeProductos(8, 3);
            Assert.AreEqual(5, result);
        }

        [Test]
        public void TestMultiplicacionProductos()
        {
            int result = Program.MultiplicacionProductos(4, 2);
            Assert.AreEqual(8, result);
        }

        [Test]
        public void TestDivisionProductos()
        {
            int result = Program.DivisionProductos(10, 2);
            Assert.AreEqual(5, result);
        }

        [Test]
        public void TestRaizCuadrada()
        {
            double result = Program.RaizCuadrada(25);
            Assert.AreEqual(5, result);
        }

        [Test]
        public void TestNumeroAlCuadrado()
        {
            double result = Program.NumeroAlCuadrado(4);
            Assert.AreEqual(16, result);
        }
    }
}
