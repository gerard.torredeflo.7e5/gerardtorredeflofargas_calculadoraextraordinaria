﻿namespace CalculadoraExtraordinaria2023
{
    public class Program
    {
        public static int SumaDeProductos(int num1, int num2)
        {
            return num1 + num2;
        }

        public static int RestaDeProductos(int num1, int num2)
        {
            return num1 - num2;
        }

        public static int MultiplicacionProductos(int num1, int num2)
        {
            return num1 * num2;
        }

        public static int DivisionProductos(int num1, int num2)
        {
            return num1 / num2;
        }

        public static double RaizCuadrada(int num)
        {
            return Math.Sqrt(num);
        }

        public static double NumeroAlCuadrado(int num)
        {
            return Math.Pow(num, 2);
        }

    }
}